<?php
$useJS=2;
$showHeader=2;
include_once "cekadmin.php";
include_once "head1.php";

$op=$nosync="";
$isOnline=false;
$statserver="-";

if (isset($_REQUEST['op'])) $op=$_REQUEST['op'];
if (isset($_REQUEST['nosync'])) $nosync=$_REQUEST['nosync'];

$urlserver="http://www.smkn57jkt.sch.id/ubk/";
$urlconfig=$urlserver."akses.txt";

$async=array(
			//judul, nama file ato folder, folder tujuan
		array("Data Mapel","cbt_mapel.txt",""),
		array("Bank Soal","cbt_paketsoal.txt",""),
		array("Butir Soal","cbt_soal.txt",""),
		array("Data Gambar","pictures/","../../pictures/"),
		array("Data Audio","audio/","../../audio/"),
		array("Data Video","video/","../../video/")		
		);
$jlhsync=count($async);

//function cekOnline() {
	if (!$sock =  @fsockopen('www.google.com', 80, $num, $error, 5)){
		$isOnline=false;
    } else 
		$isOnline=true;
//	return $isOnline;
//}

if ($isOnline) {
	$info1= "<font color=green>Online</font>";
	$sconfig = file_get_contents($urlconfig);
	//$statserver="off";
	$statserver=str_replace("sinkronisasi=","",$sconfig);
	
	$info2=" | Akses : ".($statserver=="on"?"<font color=green>Dibuka</font>":"<font color=red>Ditutup</font>");
} else  {
	$info1= "<font color=red>Offline</font>";
	$info2="";
}


function executeSqlfile($nf) {
	$t="";
	$templine = '';
	$filename=$nf;
	$lines = file($filename);
	// Loop through each line
	foreach ($lines as $line) {
		if (substr($line, 0, 2) == '--' || $line == '')		continue;
		
		$templine .= $line;
		if (substr(trim($line), -1, 1) == ';') {
			$t.="<br>$templine";
			mysql_query($templine);// or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
			$templine = '';
		}
	}
	return $t;
}

function getArrayLinkInURL($url){
	   
    // Create a new DOM Document to hold our webpage structure
    $xml = new DOMDocument();
    
    // Load the url's contents into the DOM (the @ supresses any errors from invalid XML)
    @$xml->loadHTMLFile($url);
    
    // Empty array to hold all links to return
    $links = array();
    
    //Loop through each  and  tag in the dom and add it to the link array
    foreach($xml->getElementsByTagName('a') as $link) {
    //    $links[] = array('url' => $link->getAttribute('href'), 'text' => $link->nodeValue);
		$skip=false;
		$href=$link->getAttribute('href');
		$textContent=$link->textContent;	 
		if (strstr($textContent,'Parent Directry')!='') $skip=true;
		if (strstr($href,'/')!='') $skip=true;
		if (!$skip) $links[] =$link->getAttribute('href');
    }
    
    //Return the links
    return $links;
}

function refreshData($nosync) {
	global $nsync;
	global $async;
	global $isOnline;
	global $urlserver;
	global $statserver;
	global $jlh_on,$jlh_off;
	$sync=$async[$nosync];
	$tsync="tsync".$nosync;
	$judul=$sync[0];
	$fserver=$sync[1];
	$ftarget=$sync[2];
	$jlh_on=$jlh_off=0;
	//echo $fserver;
	if (substr($fserver, -1)!="/") {
		$nmtabel=substr($fserver,0,strlen($fserver)-4);
		$jlh_off=carifield("select count(1) from $nmtabel");
		$jlh_on=" - ";
	} else {
		$afile = array_diff(scandir($ftarget), array('.', '..'));
		$jlh_off=count($afile);
		//dicari jika online dan status akses dibuka
		if (($isOnline)&&($statserver=='on')) {
			//$alink="-";
			$alink=getArrayLinkInURL($urlserver.$fserver);
			$jlh_on=count($alink);			
		} else $jlh_on= "-";
		//$t.=" Jumlah offline : $jlh_off | jumlah online: $jlh_on ";
	}
	return "$jlh_on,$jlh_off";

}

function fc_syn() {
   global $urlserver;
   global $async;
   global $isOnline;
   global $jlh_on,$jlh_off;
   
   $alc=" style='text-align:center' align='center' ";
	$ctd=" class=tdjudul align=center";
	$t="";
	$nosync=0;
	$t.="<table border=0 width=100% class='table table-striped table-bordered table-hover dataTable no-footer dtr-inline'>
		<tr>
		<th $ctd $alc width=150 >DATA</th>
		<th $ctd $alc >PR0GRESS</th>
		<th $ctd width=50 $alc>OFFLINE</th>
		<th $ctd width=50 $alc>ONLINE</th>
		<th $ctd width=50 $alc >SYNC</th>
		</tr>
	
	";
	
	$addscript="";
	foreach ($async as $sync) {
	
		$tsync="tsync".$nosync;
		$judul=$sync[0];
		$fserver=$sync[1];
		$ftarget=$sync[2];
		$jlh_on=$jlh_off=0;
	//	$t.="$judul | $fserver | $ftarget | jumlah: $jlh ";
		
		//$t.="$judul ";
		//akan dijalankan di akir
		//refreshData($nosync);
		
		$link="bukaAjax('$tsync','sinkronisasi.php?op=sync&nosync=$nosync');return false;";
		$link="executeSync($nosync);return false;";
		
		$tblink="<span><a href=# class='btn btn-primary' onclick=\"$link\" >Sinkronisasi</a></span> ";
			
		$progress="<div class='progress' style='display:none' id='progress"."$nosync'></div>";
	//	$t.="<progress id='psprogress"."$nosync' value='0'></progress>";
		$ts="<div id=$tsync > </div>";	
		
		
		$t.="
		
		<tr>
		<td width=150 >$judul</td>
		<td  >$progress $ts</td>
		<td width=50 $alc><div id=toff$nosync>$jlh_off</div></td>
		<td width=50 $alc><div id=ton$nosync>$jlh_on</div></td>
		<td width=50 $alc >$tblink</td>
		</tr>
		";

		$nosync++;
	}
	$t.="</table>";
   echo $t;
}



if ($op=='sync') {
	if (!$isOnline) {
		echo "<span class='text-danger'><i class=''></i>Tidak bisa koneksi ke internet, periksa kembali koneksi.....</span>";
		exit;
	}
	if ($statserver!='on') {
		echo "<span class='text-danger'><i class=''></i>Akses sinkronisasi ditutup...</span>";
		exit;
	}
	
	$met=ini_get('max_execution_time'); 
	ini_set('max_execution_time', 0);
	
	$t="";
	//$t.= "no sync $nosync";
	$sync=$async[$nosync];
	$judul=$sync[0];
	$fserver=$sync[1];
	$ftarget=$sync[2];
	
	$t.= "Sinkronisasi $judul ";
	if (substr($fserver, -1)!="/") {
		//jika file sql yg didownload
		//echo $t;
		//$t.=
		
		executeSqlfile($urlserver."".$fserver);
		//echo "<br><br>Finish....";	
		//exit;
	} else { 
		//jika folder yang didownload
		//hapus file offline
		$afile = array_diff(scandir($ftarget), array('.', '..'));
		foreach ($afile as $file) {
			$nf=$ftarget."/".$file;
			//$t.="<br>menghapus file $nf";
			unlink($nf);
		}		
		//echo $t;
		//exit;
		
		//download file online
		$alink=getArrayLinkInURL($urlserver.$fserver);
		$i=0;
		foreach ($alink as $link) {
			//$t.="<br>Download $link ";
			
			//$nf= pathinfo( parse_url( $link, PHP_URL_PATH ) , PATHINFO_FILENAME ); 
			$nflokal=$ftarget.$link;	
			$data = file_get_contents($urlserver.$fserver.$link);
			file_put_contents($nflokal, $data);
	//echo "$i.";
			flush();
			$i++;
		}
	} // folder
	$t.="Selesai.....";
	echo $t;
	ini_set('max_execution_time', $met);

	exit;
}

if ($op=='rd') {
	echo refreshData($nosync);
	exit;
}

 ?>
 <style>
 .isync{
	 margin:10px 0px;
	 }
.progress {
	background:url(images/pbar-ani1.gif);
	margin-top:5px;
}	
 </style>
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">
        	Sinkronisasi Bank Soal
        |  Status Server : 
		<?php 
		echo $info1.$info2;
		?>
        </h3>
    </div>
    
    <div class="col-lg-12" style="margin-top:10px;">
        <div class="panel panel-green">
            <div class="panel-heading">
                <!--a href="?modul=daftar_soal&amp;soal=" class="btn btn-default" type="button"> Refresh</a-->
                <!--p>	
                Status Server : <?php echo ($isOnline?"<font color=white>Online</font>":"<font color=red>Offline</font>"); ?>
                </p-->
            </div>
            <div class="panel-body">
                <div style="width: 100%; float:right">
                <?php
                fc_syn();
                ?>    
                <div>
            </div>
        </div>
    </div>

</div>
<script>
function executeSync(nosync){
	tsync="tsync"+nosync;
//	bukaAjax(tsync,'sinkronisasi.php?op=sync&nosync'+nosync);return false;
	$("#progress"+nosync).show();
	$.ajax({
		url: "sinkronisasi.php",
		data: "&useJS=2&op=sync&nosync="+nosync,
		cache: false,
		success: function(msg){
			$("#progress"+nosync).hide();
			$("#"+tsync).html(msg);
			scRefreshD(nosync);
		}
	});
}

function scRefreshD(nosync){
	$.ajax({
		url: "sinkronisasi.php",
		data: "&useJS=2&op=rd&nosync="+nosync,
		cache: false,
		success: function(msg){
			m=msg.split(",");
		//	$("#progress"+nosync).hide();
			$("#ton"+nosync).html(m[0]);
			$("#toff"+nosync).html(m[1]);
			
		}
	});
}

function executeSync2(nosync){
	var xhr = new XMLHttpRequest();
	var progressBar = document.getElementById("psprogress"+nosync);
	tsync="tsync"+nosync;
	url= "sinkronisasi.php?op=sync&nosync="+nosync +"&i=" + Math.floor(Math.random() * 99999);
	$("#progress"+nosync).show();
	
	xhr.open("GET", url , true);
	xhr.responseType = "text";
	xhr.onprogress = function(e) {
		if (e.lengthComputable) {
			progressBar.max = e.total;
			progressBar.value = e.loaded;
			//progressBar.innerHTML = e.loaded;
		}
	};
	xhr.onloadstart = function(e) {
		progressBar.value = 0;
		//progressBar.innerHTML="0";
	};
	xhr.onloadend = function(e) {
		progressBar.value = e.loaded;
		//progressBar.innerHTML=e.loaded;	
	};
	xhr.send(null);
}

$(document).ready(function(){
	
	for (i=0;i<=<?=$jlhsync?>;i++) {
		scRefreshD(i);
	}
	
});

</script>
</div>
</div>
